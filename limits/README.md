# Instrukcja obsługi testów na rozmiar bufora

## Za mały rozmiar bufora

Przy uruchomieniu serwera poleceniem z plikiem limits_events_1 i wysłaniu zapytania GET_EVENTS, odesłane powinno zostać 4679 opisów wydarzeń. Jeżeli rozmiar bufora jest zbyt mały, to zostanie wysłane mniej. Odkryłem również, że jeżeli rozmiar bufora ustawić na 65506 u mnie, to komunikat wysłany i przechwycony przez wireshark jest poprawny, ale klient wypisuje na sam koniec dodatkowy znak.

## Za duży rozmiar bufora

Przy uruchomieniu serwera poleceniem z plikiem limits_events_2 i wysłaniu zapytania GET_EVENTS, odesłane powinno zostać 5038 opisów wydarzeń. Jeżeli rozmiar bufora jest zbyt duży, to wysyłanie zwróci błąd ze względu na próbę zmieszczenia 5039 opisów wydarzeń, a klient nigdy nie otrzyma odpowiedzi.